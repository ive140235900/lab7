<?php

require_once 'Google/autoload.php';
require_once 'Google/Client.php';
require_once 'Google/Service/Plus.php';
session_start();

$client_id ="971565348514-ah672lgjlci2297k04175q4bc48d59r6.apps.googleusercontent.com";
$client_secret = "cAKP9ebtLPNCY-H7-PN3CPgi";
$redirect_uri = "http://localhost/lab7/google/login.php";

$client = new Google_Client();
$client->setClientId($client_id);
$client->setClientSecret($client_secret);
$client->setRedirectUri($redirect_uri);
$client->addScope("https://www.googleapis.com/auth/userinfo.profile");
$client->addScope("https://www.googleapis.com/auth/userinfo.email");

	$client = new Google_Client();
	$client->setClientId($client_id);
	$client->setClientSecret($client_secret);
	$client->setRedirectUri($redirect_uri);
	$client->addScope("https://www.googleapis.com/auth/userinfo.profile");
	$client->addScope("https://www.googleapis.com/auth/userinfo.email");

	// create an Google+ object
	$plus = new Google_Service_Plus($client);

	// "code" returned from Google, i.e. user has logged in and
	// accepted your access now you need to exchange it
	if (isset($_GET['code'])) {
		$client->authenticate($_GET['code']);
		$_SESSION['access_token'] = $client->getAccessToken();
		$redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
		header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
	}

	// We have the access token
	if (isset($_SESSION['access_token'])) {
		$client->SetAccessToken($_SESSION['access_token']);
		$_SESSION['token'] = $client->getAccessToken();
	} else {
		// The user has not authenticated. We give him a link to login
		$authUrl = $client->createAuthUrl();
	}
?>

<html>
	<head>
		<title>Login</title>
	</head>
	<body>
		<h3>OAuth2 with Google Client API</h3>

			<?php
				// User not authenticated
				if (isset($authUrl)) {
				echo "Click the image to login with Google OAuth2 <br>";
				echo "<a href='$authUrl'><img src='google.png' /></a>";
		    }

			// The access token is obtained
			if (isset($_SESSION['access_token'])) {
				$me = $plus->people->get('me');
				echo "Display Name: {$me['displayName']} <br>";

				$givenName = $me['name']['givenName'];
				echo "Given name: $givenName <br>";

				$email = ($me['emails'][0]['value']);
				echo "Email: $email <br>";
				echo "<br>";
		      
				// save $givenName to "session" to prepare for going to entry.php
				$_SESSION['givenName'] = $givenName;
		      
				echo "<a href='entry.php'>Go to my web site.</a>";
				echo "<br><br>";
		    }
		?>
	</body>
</html>
</html>